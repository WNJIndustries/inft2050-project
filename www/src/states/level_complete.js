/* globals Phaser:false */
// WNJIndustries :: 2016
// State for level complete

LevelCompleteState = {

    init: function () { },

    preload: function () { },

    create: function () {

        var BUTTON_X = this.world.centerX - 542; // X position of buttons

        // Create background and title
        this.bg = this.add.sprite(0, 0, 'bg');
        this.head = this.add.sprite(this.world.centerX, 96, 'g_lvl_complete');
		this.head.anchor.x = 0.5;

		// Create screenshot box
		this.preview = this.add.sprite(BUTTON_X, 224, 'lvl_preview');
		this.buttons = {
			lv: this.add.button(BUTTON_X + 664, 592, 'p_lvl', this.lvlCl, this),
			rs: this.add.button(BUTTON_X + 664, 496, 'p_restart', this.resCl,
			                    this)
		};
		this.buttons.lv.anchor.y = 1;
		this.buttons.rs.anchor.y = 1;

		this.tx = this.add.text(BUTTON_X + 874, 400, "Your score:\n" +
		                        game.score);
		this.tx.align = 'center';
		this.tx.fontSize = 48;
		this.tx.fill = "#fff";
		this.tx.anchor.x = 0.5;
		this.tx.anchor.y = 1;

		// Create and populate score text group
		this.score_group = this.add.group();

		this.lv = game.level - 1;

		var pos = -1;

		for (var i = 4; i >= 0; i--) {

			if (game.score > game.scores[this.lv][i]) {
				pos = i;
			} else {
				break;
			}
		}

		if (pos > -1) {
			for (var i = 4; i > pos; i--) {
				game.scores[this.lv][i] = game.scores[this.lv][i - 1];
			}
			game.scores[this.lv][pos] = game.score;
		}

		// Load all five scores to group
		for (var i = 0; i < 5; i++) {

			var phrase, txt;

			// Determine score text via page number and index
			phrase = (i + 1) + ': ' + game.scores[this.lv][i];

			// Create text at correct posiiton and size
			txt = this.add.text(BUTTON_X + 16, 240 + 72 * i, phrase);
			txt.fontSize = 40; txt.fill = '#fff';

			// Add text to scores group
			this.score_group.add(txt);
		};
    },

	update: function () { },

	goBack: function() {

		// Return to main menu
		playSFX(game.sfx.click);
		game.state.start('menu');
	},

    // Change apge based on click functions
    selectPage: function() {

		// Determine state variable
		var state = game.state.states.scores;

		// Determine level number
		state.key = Number(this.key.substr(3));

		// Change level selection indicator position
		state.indicator.y = 220 + (state.key - 1) * 96;

		state.changeScores(state.key - 1);
		playSFX(game.sfx.click);
	},

	lvlCl: function() {
		playSFX(game.sfx.click);
		game.state.start('level_select');
	},

	resCl: function() {
		playSFX(game.sfx.click);
		game.state.start('play');
	}
};
