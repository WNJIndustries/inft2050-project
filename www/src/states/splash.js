/* globals Phaser:false */
// WNJIndustries :: 2016
// State for the splash

SplashState = {

    init: function () { },

    preload: function () { },

    create: function () {

        // Create background and logo
        this.splash = this.add.sprite(0, 0, 'splash');

        window.setTimeout(function() {
            game.state.start('menu');
        }, 1000);
    },

	update: function () { }
};
