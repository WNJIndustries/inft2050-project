/* globals Phaser:false */
// WNJIndustries :: 2016
// State for the scores

ScoresState = {

    init: function () { },

    preload: function () { },

    create: function () {

        var BUTTON_X = this.world.centerX - 372; // X position of buttons

        // Create background and title
        this.bg = this.add.sprite(0, 0, 'bg');
        this.head = this.add.sprite(this.world.centerX - 285, 96, 'hsc_head');

		// Back button
		this.back = this.add.button(40, 40, 'back', this.goBack);

		// Record current position of indicator and create indicator
		this.key = 1;
		this.indicator = this.add.sprite(BUTTON_X - 4, 220, 'lvl_light');

		// Create screenshot box
		this.preview = this.add.sprite(BUTTON_X + 96, 224, 'lvl_preview');

        // Add buttons with functions
        this.button = {
            lvl1: this.add.button(BUTTON_X, 224, 'lvl1', this.selectPage),
            lvl2: this.add.button(BUTTON_X, 320, 'lvl2', this.selectPage),
            lvl3: this.add.button(BUTTON_X, 416, 'lvl3', this.selectPage),
            lvl4: this.add.button(BUTTON_X, 512, 'lvl4', this.selectPage)
        };

		// Create and populate score text group
		this.score_group = this.add.group();
		this.changeScores(0);
    },

	update: function () { },

	goBack: function() {

		// Return to main menu
		playSFX(game.sfx.click);
		game.state.start('menu');
	},

    // Change apge based on click functions
    selectPage: function() {

		// Determine state variable
		var state = game.state.states.scores;

		// Determine level number
		state.key = Number(this.key.substr(3));

		// Change level selection indicator position
		state.indicator.y = 220 + (state.key - 1) * 96;

		state.changeScores(state.key - 1);
		playSFX(game.sfx.click);
	},

	changeScores: function(page) {

		// Determine state variable
		var state = game.state.states.scores;

		// Remove all scores from group
		state.score_group.removeAll();

		// Load all five scores to group
		for (var i = 0; i < 5; i++) {

			var phrase, txt;

			// Determine score text via page number and index
			phrase = (i + 1) + ': ' + game.scores[page][i];

			// Create text at correct posiiton and size
			txt = state.add.text(380, 240 + 72 * i, phrase);
			txt.fontSize = 40; txt.fill = '#fff';

			// Add text to scores group
			state.score_group.add(txt);
		};
	}
};
