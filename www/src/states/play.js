/* globals Phaser:false */
// WNJIndustries :: 2016
// State in which gameplay actually occurs

PlayState = {

    init: function () { },

    preload: function () { },

    create: function () {

		var BUTTON_X = this.world.centerX - 210; // X position of buttons

		this.is_paused = false;  // Whether or not the game is paused
		this.is_sleeping = false;
		this.BOUNCE = 0.6;       // How much the ball will bounce off walls
		this.DRAG = 224;         // Drag to be placed on the ball so it slows
		this.SPEED = 32;         // Speed of acceleration of ball allowed
		this.FIRE_RATE = 1600;    // How many ms between bullets firing

		this.level = game.level;

		// Start physics system
		this.physics.startSystem(PHYSICS_MODE);

		// Modify TILE_BIAS so we're not phasing (GET IT???) through walls
		this.physics.arcade.TILE_BIAS = 40;

		// Tilemap loading
		this.map = this.add.tilemap('lvl' + this.level);
		this.map.addTilesetImage('bg', 'g_bg' + this.level);
		this.map.addTilesetImage('wall_default', 'g_wall_default');
		this.map.addTilesetImage('turret_4way', 'g_turret');
		this.m_bg = this.map.createLayer('background');
		this.m_static = this.map.createLayer('static_layer');
		this.m_static.resizeWorld();

		this.coins = this.add.group();
		this.coins.enableBody = true;
		this.coins.physicsBodyType = PHYSICS_MODE;
		this.map.createFromObjects('coins', 10, 'g_coin', 0, true, false,
		                           this.coins);
		this.coins_taken = 0;
		this.coins_start = this.coins.children.length;

		// Help with bullet firing retrieved from:
		// http://phaser.io/examples/v2/arcade-physics/group-vs-group
		this.bullets = this.add.group();
		this.bullets.enableBody = true;
		this.bullets.physicsBodyType = PHYSICS_MODE;
		for (var i = 0; i < 40; i++) {
			var b = this.bullets.create(0, 0, 'g_bullet');
			b.name = 'bullet' + i;
			b.exists = false;
			b.visible = false;
        	b.checkWorldBounds = true;
			b.anchor.setTo(0.5, 0.5);
			b.events.onOutOfBounds.add(this.resetBullet, this);
		}

		this.turrets = this.add.group();
		this.turrets.enableBody = true;
		this.turrets.physicsBodyType = PHYSICS_MODE;
		if (game.level > 2) {
			var result = findObjectsByType(this.map, 'turrets');
			result.forEach(function(element) {
				createFromTiledObject(element, this.turrets);
			}, this);

			this.turrets.forEach(function(turret) {
				turret.loadTexture('g_turret');
				turret.body.immovable = true;
				turret.body.moves = false;
				turret.body.setSize(80, 80);
			});
			this.bulletTimer = 0;
		}

		console.log(findObjectsByType(this.map, 'coins'));

		this.doors = this.add.group();
		this.doors.enableBody = true;
		this.doors.physicsBodyType = PHYSICS_MODE;
        this.map.createFromObjects('red_door', 5, 'g_wall_red', 0, true,
		                           false, this.doors);
		this.map.createFromObjects('blue_door', 11, 'g_wall_blue', 0, true,
		                           false, this.doors);
		this.doors.forEach(function(door) {
			door.body.immovable = true;
			door.body.moves = false;
		});

   		this.exits = this.add.group();
   		this.exits.enableBody = true;
   		this.exits.physicsBodyType = PHYSICS_MODE;
        this.map.createFromObjects('exit', 6, 'g_exit', 0, true,
   		                           false, this.exits);

  		this.fires = this.add.group();
  		this.fires.enableBody = true;
  		this.fires.physicsBodyType = PHYSICS_MODE;
		this.map.createFromObjects('fire', 13, 'g_fire', 0, true,
  		                           false, this.fires);

		this.switches = this.add.group();
		this.switches.enableBody = true;
		this.switches.physicsBodyType = PHYSICS_MODE;
        this.map.createFromObjects('red_switch', 4, 'g_sw_red_up', 0, true,
		                           false, this.switches);
		this.map.createFromObjects('blue_switch', 12, 'g_sw_blue_up', 0, true,
		                           false, this.switches);

		this.water = this.add.group();
		this.water.enableBody = true;
		this.water.physicsBodyType = PHYSICS_MODE;
        this.map.createFromObjects('water', 8, 'g_water', 0, true, false,
		                           this.water);
		this.water.forEach(function(item) {
			item.animations.add('ripple');
			item.animations.play('ripple', 1, true);
		});

		this.map.setCollisionBetween(1, 1000, true, "static_layer");

        // Create ball object
		this.t = this.add.group();
        this.map.createFromObjects('ball', 3, 'g_ball', 0, true, false, this.t);
		this.ball = this.t.children[0];

        // Enable physics and set drag + bounce of ball object
        this.physics.enable(this.ball, PHYSICS_MODE);
        this.ball.body.drag.x = this.DRAG;
        this.ball.body.drag.y = this.DRAG;
        this.ball.body.bounce.x = this.BOUNCE;
        this.ball.body.bounce.y = this.BOUNCE;
		this.ball.body.maxVelocity = 32;

		// Back button
		this.btn_pause = this.add.button(4, 4, 'g_pause', this.pause, this);

		this.gui_coins = this.add.text(640, 4, 'Coins: 0/' + this.coins_start);
		this.gui_coins.anchor.x = 0.5;
		this.gui_coins.fill = '#fff';
		this.gui_coins.fontSize = 64;
		this.gui_coins.stroke = '#000';
		this.gui_coins.strokeThickness = 8;

		this.gui_time = this.add.text(1276, 4, '0:00');
		this.gui_time.anchor.x = 1;
		this.gui_time.fill = '#fff';
		this.gui_time.fontSize = 64;
		this.gui_time.stroke = '#000';
		this.gui_time.strokeThickness = 8;

		this.time = game.times[this.level - 1];


		this.pause_gui = this.add.group();
		this.pause_gui.add(this.add.sprite(0, 0, 'p_bg'));
        this.pause_gui.add(this.add.sprite(this.world.centerX - 285, 96,
		                   'p_head'));

		// Add buttons with functions
		this.pause_gui.add(this.add.button(BUTTON_X, 272, 'p_resume',
		                  this.resumeCl, this));
		this.pause_gui.add(this.add.button(BUTTON_X, 368, 'p_restart',
		                  this.restartCl, this));
		this.pause_gui.add(this.add.button(BUTTON_X, 464, 'p_lvl',
	                      this.selectCl, this));

		this.sfx = {
			alarm: this.add.audio('alarm'),
			coin: this.add.audio('coin'),
			finish: this.add.audio('finish'),
			fire: this.add.audio('fire'),
			laser_collision: this.add.audio('laser_collision'),
			laser_firing: this.add.audio('laser_firing'),
			level_start: this.add.audio('level_start'),
			splash: this.add.audio('splash'),
			sw: this.add.audio('sw')
		};

		this.sfx.alarm.onStop.add(this.restartLevel, this);
		this.sfx.fire.onStop.add(this.restartLevel, this);
		this.sfx.laser_collision.onStop.add(this.restartLevel, this);
		this.sfx.splash.onStop.add(this.restartLevel, this);

		playSFX(this.sfx.level_start);
    },

	update: function () {

		var dt = (game.time.elapsedMS / 1000);

		// Enable or disable physics / graphics based on game pausedness
		this.physics.arcade.isPaused = this.is_paused || this.is_sleeping;
		this.btn_pause.visible = !this.is_paused;
		this.pause_gui.visible = this.is_paused;

		if (!this.is_paused && !this.is_sleeping) {

	        var sign;

	        // Calculate collisions between ball and all blocks
	        this.physics.arcade.collide(this.ball, this.m_static);
	        this.physics.arcade.collide(this.ball, this.doors);
	        this.physics.arcade.collide(this.ball, this.turrets);

			this.physics.arcade.overlap(this.ball, this.coins,
			                            this.coinsCollision, null, this);
			this.physics.arcade.overlap(this.ball, this.exits,
			                            this.exitsCollision, null, this);
			this.physics.arcade.overlap(this.ball, this.fires,
			                            this.firesCollision, null, this);
			this.physics.arcade.overlap(this.ball, this.switches,
			                            this.switchesCollision, null, this);
			this.physics.arcade.overlap(this.ball, this.water,
			                            this.waterCollision, null, this);
			this.physics.arcade.overlap(this.ball, this.bullets,
			                            this.bulletsCollision, null, this);


			if (game.level > 2) {

				this.bulletTimer -= (dt * 1000);

				if (this.bulletTimer < 0) {
					this.bulletTimer += this.FIRE_RATE;
					this.fireBullet(this);
				}
			}

			this.physics.arcade.overlap(this.bullets, this.m_static, this.resetBullet, null, this);

			// Calculate whether screen is pointing towards floor or sky
			sign = Math.sign(accel.z);

			// Move logo based on acceleration
			this.ball.body.velocity.x += accel.y * this.SPEED * sign;
			this.ball.body.velocity.y += accel.x * this.SPEED * sign;

			// Decrease timer and restart if below 0 seconds left
			this.time -= dt;
			if (this.time < 0) {
				playSFX(this.sfx.alarm);
				this.is_sleeping = true;
			};

			// Update GUI elements
			this.gui_coins.text = 'Coins: ' + this.coins_taken + '/' +
			                      this.coins_start;
			this.gui_time.text = String(Math.ceil(this.time)).toMMSS();
		}
	},

	pause: function() {

		// Pause game
		playSFX(game.sfx.click);
		this.is_paused = true;
	},

	bulletsCollision: function(player, bullet) {

		playSFX(this.sfx.laser_collision);
		this.is_sleeping = true;
	},

	coinsCollision: function(player, coin) {

		playSFX(this.sfx.coin);

		// Increase collected coin count
		this.coins_taken++;

		// Kill coin
		coin.kill();
	},

	exitsCollision: function(player, portal) {

		// End the level
		playSFX(this.sfx.finish);
		game.score = Math.ceil(this.time) * 250 + this.coins_taken * 1000;
		game.state.start('level_complete');
	},

	firesCollision: function(player, fire) {

		// Death restarts level
		this.is_sleeping = true;
		playSFX(this.sfx.fire);
	},

	fireBullet: function(state) {

		playSFX(state.sfx.laser_firing);

		state.turrets.forEach(function(turret) {

			state.bullet = state.bullets.getFirstExists(false);

	        if (state.bullet)
	        {
	            state.bullet.reset(turret.x + 40, turret.y + 40);
				if (turret.dir == "up") {
	            	state.bullet.body.velocity.y = -200;
					state.bullet.angle = 0;
				} else if (turret.dir == "down") {
					state.bullet.body.velocity.y = 200;
					state.bullet.angle = 180;
				} else if (turret.dir == "left") {
					state.bullet.body.velocity.x = -200;
					state.bullet.angle = 270;
				}
	        }
		});
	},

	switchesCollision: function(player, sw) {

		// Determine switch information
		var sw_info = sw.key.split('_');

		// If switch is not yet pressed...
		if (sw_info[3] == "up") {

			playSFX(this.sfx.sw);

			// Modify switch sprite
			sw_info[3] = "down";
			sw.loadTexture(sw_info.join('_'));

			// Open door
			this.doors.forEach(function(door) {
				if (door.key.split('_')[2] == sw_info[2]) {

					door.loadTexture("g_wall_open");
					door.body.enable = false;
				};
			});
		};
	},

	resetBullet: function(bullet, wall) {

		if (wall) {
			if (wall.index > -1) {
				bullet.kill();
			}
		}
	},

	waterCollision: function(player, water) {

		// Death restarts level
		this.is_sleeping = true;
		playSFX(this.sfx.splash);
	},

	restartLevel: function() {

		this.state.restart();
	},

	resumeCl: function() {
		playSFX(game.sfx.click);
		this.is_paused = false;
	},
	restartCl: function() {
		playSFX(game.sfx.click);
		this.state.restart();
	},
	selectCl: function() {
		playSFX(game.sfx.click);
		this.state.start('level_select');
	},
};
