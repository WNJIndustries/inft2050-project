/* globals Phaser:false */
// WNJIndustries :: 2016
// State that set core game variables

InitState = {

    init: function () {

        // Limit number of touch inputs to 1
        this.input.maxPointers = 1;

        // Scale game while maintaining aspect ratio
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        // Align game to centre of screen
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        // Force game into landscape orientation and correctly set scaling modes
        this.scale.forceOrientation(true, false);
        this.scale.updateLayout(true);
        this.scale.refresh();
    },

    preload: function () {

		// Load graphical assets

		// General
		this.load.image('back', 'asset/gfx/back.png');
        this.load.image('bg', 'asset/gfx/menu/bg.jpg');
        this.load.image('logo', 'asset/gfx/menu/logo.png');

		// Main menu
        this.load.image('b_credits', 'asset/gfx/menu/button_credits.png');
        this.load.image('b_options', 'asset/gfx/menu/button_options.png');
        this.load.image('b_scores', 'asset/gfx/menu/button_scores.png');
        this.load.image('b_start', 'asset/gfx/menu/button_start.png');

		// Credits screen
        this.load.image('cdt_body', 'asset/gfx/credits/body.png');
        this.load.image('cdt_head', 'asset/gfx/credits/title.png');

        // Level select screen
        this.load.image('lvl1', 'asset/gfx/lvl_select/lvl1.png');
        this.load.image('lvl2', 'asset/gfx/lvl_select/lvl2.png');
        this.load.image('lvl3', 'asset/gfx/lvl_select/lvl3.png');
        this.load.image('lvl4', 'asset/gfx/lvl_select/lvl4.png');
        this.load.image('lvl_light', 'asset/gfx/lvl_select/lvl_light.png');
        this.load.image('lvl_play', 'asset/gfx/lvl_select/lvl_play.png');
        this.load.image('lvl_preview', 'asset/gfx/lvl_select/lvl_preview.png');
        this.load.image('lvl_head', 'asset/gfx/lvl_select/title.png');
        this.load.image('lvl_scr1', 'asset/gfx/lvl_select/lvl_scr1.jpg');
        this.load.image('lvl_scr2', 'asset/gfx/lvl_select/lvl_scr2.jpg');
        this.load.image('lvl_scr3', 'asset/gfx/lvl_select/lvl_scr3.jpg');
        this.load.image('lvl_scr4', 'asset/gfx/lvl_select/lvl_scr4.jpg');

		// Options screen
        this.load.image('opt_head', 'asset/gfx/options/title.png');
        this.load.image('opt_slider', 'asset/gfx/options/slider.png');
        this.load.image('opt_slider_bar', 'asset/gfx/options/slider_bar.png');
        this.load.image('opt_volume', 'asset/gfx/options/volume.png');

		// Scores screen
        this.load.image('hsc_head', 'asset/gfx/scores/title.png');

        // Splash screen
        this.load.image('splash', 'asset/gfx/splash.jpg');

		// Gameplay sprites
        this.load.image('g_ball',  'asset/gfx/play/ball.png');
        this.load.image('g_bg1',  'asset/gfx/play/bg2.jpg');
        this.load.image('g_bg2',  'asset/gfx/play/bg3.jpg');
        this.load.image('g_bg3',  'asset/gfx/play/bg4.jpg');
        this.load.image('g_bg4',  'asset/gfx/play/bg5.jpg');
        this.load.image('g_bullet',  'asset/gfx/play/turret_bullet.png');
        this.load.image('g_coin',  'asset/gfx/play/coin.png');
        this.load.spritesheet('g_exit',  'asset/gfx/play/exit.png', 80, 80);
        this.load.image('g_fire',  'asset/gfx/play/fire.png');
        this.load.image('g_lvl_complete',  'asset/gfx/play/level_complete.png');
        this.load.image('g_pause',  'asset/gfx/play/pause.png');
        this.load.image('g_sw_blue_down',  'asset/gfx/play/sw_blue_down.png');
        this.load.image('g_sw_blue_up',  'asset/gfx/play/sw_blue_up.png');
        this.load.image('g_sw_red_down',  'asset/gfx/play/sw_red_down.png');
        this.load.image('g_sw_red_up',  'asset/gfx/play/sw_red_up.png');
        this.load.image('g_turret',  'asset/gfx/play/turret_4way.png');
        this.load.image('g_wall_blue', 'asset/gfx/play/wall_blue.png');
        this.load.image('g_wall_default', 'asset/gfx/play/wall_default.png');
        this.load.image('g_wall_open', 'asset/gfx/play/wall_open.png');
        this.load.image('g_wall_red', 'asset/gfx/play/wall_red.png');
        this.load.spritesheet('g_water',  'asset/gfx/play/water.png', 80, 80);

		// Paused sprites
		this.load.image('p_bg', 'asset/gfx/pause/bg.png');
		this.load.image('p_lvl', 'asset/gfx/pause/button_lvl.png');
		this.load.image('p_restart', 'asset/gfx/pause/button_restart.png');
		this.load.image('p_resume', 'asset/gfx/pause/button_resume.png');
		this.load.image('p_head', 'asset/gfx/pause/title.png');

		// Load tilemaps
		var MAP_TYPE = Phaser.Tilemap.TILED_JSON;
		this.load.tilemap('lvl1', 'asset/maps/lvl1.json', null, MAP_TYPE);
		this.load.tilemap('lvl2', 'asset/maps/lvl2.json', null, MAP_TYPE);
		this.load.tilemap('lvl3', 'asset/maps/lvl3.json', null, MAP_TYPE);
		this.load.tilemap('lvl4', 'asset/maps/lvl4.json', null, MAP_TYPE);


		// Load sound effects
		this.load.audio('alarm', 'asset/sfx/alarm.ogg');
		this.load.audio('click', 'asset/sfx/click.ogg');
		this.load.audio('coin', 'asset/sfx/coin.ogg');
		this.load.audio('finish', 'asset/sfx/finish.ogg');
		this.load.audio('fire', 'asset/sfx/fire.ogg');
		this.load.audio('laser_collision', 'asset/sfx/laser_collision.ogg');
		this.load.audio('laser_firing', 'asset/sfx/laser_firing.ogg');
		this.load.audio('level_start', 'asset/sfx/level_start.ogg');
		this.load.audio('splash', 'asset/sfx/splash.ogg');
		this.load.audio('switch', 'asset/sfx/switch.ogg');
	},

    create: function () {

		// Saved scores for all four levels
		game.scores = [
			[ 22000, 20000, 18000, 14000, 10000 ],
			[ 22000, 20000, 18000, 14000, 10000 ],
			[ 22000, 20000, 18000, 14000, 10000 ],
			[ 22000, 20000, 18000, 14000, 10000 ]
		];

		game.level = 1;

		game.score = 0;

		game.sfx = {
			click: game.add.audio('click')
		};

		game.volume = 1;

		game.times = [ 60, 60, 75, 90 ];

        // Start the game proper
        game.state.start('splash');
    },

	update: function () { }
};

String.prototype.toMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var minutes = Math.floor((sec_num) / 60);
    var seconds = sec_num- (minutes * 60);

    if (seconds < 10) {seconds = "0"+seconds;}
    return minutes+':'+seconds;
};

function playSFX(sound) {
	sound.play('', 0, game.volume);
}

function findObjectsByType(map, layer) {
	var result = new Array();
	map.objects[layer].forEach(function(element) {
		//Phaser uses top left, Tiled bottom left so we have to adjust
		//also keep in mind that the cup images are a bit smaller than the tile which is 16x16
		//so they might not be placed in the exact position as in Tiled
		element.y -= map.tileHeight;
		result.push(element);
	});
	return result;
}

// Create a sprite from an object
function createFromTiledObject(element, group) {

	var sprite = group.create(element.x, element.y, element.properties.sprite);

	//copy all properties to the sprite
	Object.keys(element.properties).forEach(function(key){
		sprite[key] = element.properties[key];
	});
}
