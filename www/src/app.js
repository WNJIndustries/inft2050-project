/* globals Phaser:false, BasicGame: false */
// WNJIndustries :: 2016
// Create our game!

var accel, game, PHYSICS_MODE;

// Holds accelerometer values
accel = { x: 0, y: 0, z: 0 };

// Update accelerometer upon device motion
window.ondevicemotion = function (event) {
	accel.x = event.accelerationIncludingGravity.x / 10;
	accel.y = event.accelerationIncludingGravity.y / 10;
	accel.z = event.accelerationIncludingGravity.z / 9.81;
};

// Set will use arcade physics for this game as it is fast
PHYSICS_MODE = Phaser.Physics.ARCADE;

// Create game with 720p resolution
game = new Phaser.Game(1280, 720, Phaser.AUTO, 'game');

// Load all game states
game.state.add('credits', CreditsState);
game.state.add('init', InitState);
game.state.add('level_complete', LevelCompleteState);
game.state.add('level_select', LevelSelectState);
game.state.add('menu', MenuState);
game.state.add('options', OptionsState);
game.state.add('play', PlayState);
game.state.add('scores', ScoresState);
game.state.add('splash', SplashState);

// Load our initialisation state
game.state.start('init');
